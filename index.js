const signUpButtom = document.getElementById("signUp");
const signInButtom = document.getElementById("signIn");
const container = document.getElementById("container");

signUpButtom.addEventListener("click", () => {
  container.classList.add("right-panel-active");
});

signInButtom.addEventListener("click", () => {
  container.classList.remove("right-panel-active");
});

document.getElementById('Fsignin').addEventListener('submit', (e)=>{
  let mail = document.getElementById('IMail').value;
  let pass = document.getElementById('IPass').value;
  fetch('https://api.acamica-bbg-restdelilah.ga/login/log', {
      method: 'POST',
      body: JSON.stringify({
        email: mail,
        pass: pass
      }),
      mode: 'cors',
      headers: {
        "Content-type": "application/json",
        'Access-Control-Allow-Origin': '*'
      }
  })
    .then(response => response.json())
    .then(json => {
      location.replace(`https://api.acamica-bbg-restdelilah.ga//api?${json}`);
    })
});

document.getElementById('Fsignup').addEventListener('submit', ()=>{
  let mail = document.getElementById('rMail').value;
  let pass = document.getElementById('rPass').value;
  let name = document.getElementById('rName').value;
  let lastname = document.getElementById('rLName').value;
  let dir = document.getElementById('rLDir').value;
  fetch('https://api.acamica-bbg-restdelilah.ga/register/add', {
      method: 'POST',
      body: JSON.stringify({
        email: mail,
        pass: pass,
        name: name,
        lastname: lastname,
        dir: [
          dir
        ]
      }),
      mode: 'cors',
      headers: {
        "Content-type": "application/json",
        'Access-Control-Allow-Origin': '*'
      }
  })
    .then(response => response.json())
    .then(json => console.log(json))
});